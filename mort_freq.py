import os
import re
import pandas as pd
from pathlib import Path

age_of_death = []
mort_fraq = []
count = []
population = 0


def import_data(path):
    print(path)
    global age_of_death, population

    with open(path) as data_file:
        data = data_file.readline()
        while data:
            age_of_death.append(data.strip('\n').split('\t')[2])
            data = data_file.readline()

    os.makedirs(os.path.dirname('mortfreq_output/'), exist_ok=True)
    os.makedirs(os.path.dirname('mortfraq_output/'), exist_ok=True)
    os.makedirs(os.path.dirname('survfreq_output/'), exist_ok=True)
    os.makedirs(os.path.dirname('survfraq_output/'), exist_ok=True)

    population = len(age_of_death)


def freq_table():
    data_frame = pd.DataFrame({'age_of_death': age_of_death})
    data_frame = pd.value_counts(data_frame.age_of_death).to_frame().reset_index()
    data_frame.columns = ['age_of_death', 'count']
    data_frame.sort_values('age_of_death', inplace=True)

    to_list(data_frame)


def fraq_table():
    current_population = population
    mortfraq_file = open("mortfraq_output/%s.mortfraq" % strain_name, "w+")
    mortfraq_file.write("{}\t{}\n".format(0, 0))
    survfraq_file = open("survfraq_output/%s.survfraq" % strain_name, "w+")
    survfraq_file.write("{}\t{}/{}\n".format(0, population, population))

    for index in range(len(age_of_death)):
        current_population -= count[index]
        mortfraq_file.write("%s\t%s\n" % (age_of_death[index], "{}/{}".format(count[index], population)))
        survfraq_file.write("%s\t%s\n" % (age_of_death[index], "{}/{}".format(current_population, population)))


def to_list(data_frame):
    global age_of_death, count, strain_name
    current_population = population
    mortfreq_file = open("mortfreq_output/%s.mortfreq" % strain_name, "w+")
    mortfreq_file.write("{}\t{}\n".format(0, 0))
    survfreq_file = open("survfreq_output/%s.survfreq" % strain_name, "w+")
    survfreq_file.write("{}\t{}\n".format(0, population))
    age_of_death = data_frame['age_of_death'].values.tolist()
    count = data_frame['count'].values.tolist()

    for index in range(len(age_of_death)):
        current_population -= count[index]
        mortfreq_file.write("%s\t%s\n" % (age_of_death[index], count[index]))
        survfreq_file.write("%s\t%s\n" % (age_of_death[index], current_population))


    fraq_table()


path_list = Path('records').glob('**/*.txt')
for path in path_list:
    path_in_str = str(path)
    strain_name = (re.search(r'(?<=/)(.*)(?=.txt)', path_in_str)).group()

    import_data(path_in_str)
    freq_table()
